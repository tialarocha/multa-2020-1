package br.ucsal.testequalidade20162.domain;

import java.time.LocalDateTime;

public class Multa {

	private LocalDateTime dataHora;

	private String local;

	private TipoMultaEnum tipo;

	public LocalDateTime getDataHora() {
		return dataHora;
	}

	public void setDataHora(LocalDateTime dataHora) {
		this.dataHora = dataHora;
	}

	public String getLocal() {
		return local;
	}

	public void setLocal(String local) {
		this.local = local;
	}

	public TipoMultaEnum getTipo() {
		return tipo;
	}

	public void setTipo(TipoMultaEnum tipo) {
		this.tipo = tipo;
	}

	@Override
	public String toString() {
		return "Multa [dataHora=" + dataHora + ", local=" + local + ", tipo=" + tipo + "]";
	}

}
